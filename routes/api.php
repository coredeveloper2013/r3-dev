<?php
Route::group([
    'middleware' => ['api'],
    'prefix' => 'auth'

], function () {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

// Route::group([
//     'middleware' => ['jwt.auth'],
//     'prefix' => 'admin'
// ], function () {
//     Route::resource('settings', 'Settings\SettingController')->except([
//         'create', 'edit', 'delete'
//     ]);
// });
